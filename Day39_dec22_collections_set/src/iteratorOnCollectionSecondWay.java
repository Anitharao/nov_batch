import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class iteratorOnCollectionSecondWay {
	public static void main(String[] args) {
		ArrayList<String> namesList = new ArrayList<>(Arrays.asList("Mark", "Alpha", "Delta", "Charlie", "Roger"));

		Iterator<String> itr = namesList.iterator();
		itr.forEachRemaining(e -> {
			itr.remove();
		});

		System.out.println(namesList); // []
	}
}
